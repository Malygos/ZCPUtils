//
//  ZCPIndicator.m
//  ZCPKit
//
//  Created by zhuchaopeng on 16/9/18.
//  Copyright © 2016年 zcp. All rights reserved.
//

#import "ZCPIndicator.h"

static NSInteger loadingCount;

@implementation ZCPIndicator

#pragma mark - public methods
+ (void)showIndicator {
    // 显示状态栏菊花
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

+ (void)dismissIndicator {
    //隐藏状态栏菊花
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

+ (void)stopLoading {
    loadingCount --;
    if (loadingCount == 0) {
        [self dismissIndicator];
    }
}

@end
