Pod::Spec.new do |spec|

  spec.name          = "ZCPUtil"
  spec.version       = "0.0.1"
  spec.author        = { "朱超鹏" => "z164757979@163.com" }
  spec.license       = { :type => 'MIT', :file => 'LICENSE' }
  spec.homepage      = "https://gitlab.com/Malygos/ZCPUtil"
  spec.source        = { :git => "https://gitlab.com/Malygos/ZCPUtil.git", :tag => "#{spec.version}" }
  spec.summary       = "ZCP ZCPUtil."
  spec.description   = <<-DESC
                       ZCP ZCPUtil. pa hao fang router
                       DESC

  spec.platform      = :ios, '9.0'
  spec.ios.deployment_target = '9.0'
  spec.module_name   = 'ZCPUtil'
  spec.framework     = 'Foundation', 'UIKit'

  if ENV['IS_SOURCE']
    spec.source_files  = "Source/**/*.{h,m}"
  end

  if ENV['IS_FRAMEWORK']
    spec.vendored_frameworks = 'Framework/ZCPUtil.framework'
  end

end
