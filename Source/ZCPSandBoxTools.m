//
//  ZCPSandBoxTools.m
//  ZCPUtil
//
//  Created by zcp on 2019/5/20.
//  Copyright © 2019 zcp. All rights reserved.
//

#import "ZCPSandBoxTools.h"

/// ~
NSString *NSSandBoxHomePath() {
    return NSHomeDirectory();
}
/// /
NSString *NSSystemRootPath() {
    return NSOpenStepRootDirectory();
}


/// ~/Documents
NSString *NSSandBoxDocumentsPath() {
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
}
/// ~/Library
NSString *NSSandBoxLibraryPath() {
    return [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) firstObject];
}
/// ~/Library/Caches
NSString *NSSandBoxCachesPath() {
    return [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
}
/// ~/Library/Preferences
NSString *NSSandBoxPreferencesPath() {
    return [NSString stringWithFormat:@"%@/Preferences", NSSandBoxLibraryPath()];
}
/// ~/Library/Application Support
NSString *NSSandBoxApplicationSupportPath() {
    return [NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES) firstObject];
}
/// ~/Library/Documentation
NSString *NSSandBoxDocumentationPath() {
    return [NSSearchPathForDirectoriesInDomains(NSDocumentationDirectory, NSUserDomainMask, YES) firstObject];
}
/// ~/Library/Autosave Information
NSString *NSSandBoxAutosaveInformationPath() {
    return [NSSearchPathForDirectoriesInDomains(NSAutosavedInformationDirectory, NSUserDomainMask, YES) firstObject];
}
/// ~/Library/Input Methods
NSString *NSSandBoxInputMethodsPath() {
    return [NSSearchPathForDirectoriesInDomains(NSInputMethodsDirectory, NSUserDomainMask, YES) firstObject];
}
/// ~/Library/PreferencePanes
NSString *NSSandBoxPreferencePanesPath() {
    return [NSSearchPathForDirectoriesInDomains(NSPreferencePanesDirectory, NSUserDomainMask, YES) firstObject];
}
/// ~/tmp/
NSString *NSSandBoxTmpPath() {
    return NSTemporaryDirectory();
}


/// ~/Applications
NSString *NSSandBoxApplicationsPath() {
    return [NSSearchPathForDirectoriesInDomains(NSApplicationDirectory, NSUserDomainMask, YES) firstObject];
}
/// ~/Applications/Demos
NSString *NSSandBoxDemosPath() {
    return [NSSearchPathForDirectoriesInDomains(NSDemoApplicationDirectory, NSUserDomainMask, YES) firstObject];
}
/// ~/Applications/Utilities
NSString *NSSandBoxUtilitiesPath() {
    return [NSSearchPathForDirectoriesInDomains(NSAdminApplicationDirectory, NSUserDomainMask, YES) firstObject];
}
/// ~/Developer/Applications
NSString *NSSandBoxDeveloperApplicationPath() {
    return [NSSearchPathForDirectoriesInDomains(NSDeveloperApplicationDirectory, NSUserDomainMask, YES) firstObject];
}
/// ~/Developer
NSString *NSSandBoxDeveloperPath() {
    return [NSSearchPathForDirectoriesInDomains(NSDeveloperDirectory, NSUserDomainMask, YES) firstObject];
}


/// ~/Desktop
NSString *NSSandBoxDesktopPath() {
    return [NSSearchPathForDirectoriesInDomains(NSDesktopDirectory, NSUserDomainMask, YES) firstObject];
}
/// ~/Downloads
NSString *NSSandBoxDownloadsPath() {
    return [NSSearchPathForDirectoriesInDomains(NSDownloadsDirectory, NSUserDomainMask, YES) firstObject];
}
/// ~/Movies
NSString *NSSandBoxMoviesPath() {
    return [NSSearchPathForDirectoriesInDomains(NSMoviesDirectory, NSUserDomainMask, YES) firstObject];
}
/// ~/Music
NSString *NSSandBoxMusicPath() {
    return [NSSearchPathForDirectoriesInDomains(NSMusicDirectory, NSUserDomainMask, YES) firstObject];
}
/// ~/Pictures
NSString *NSSandBoxPicturesPath() {
    return [NSSearchPathForDirectoriesInDomains(NSPicturesDirectory, NSUserDomainMask, YES) firstObject];
}
/// ~/Public
NSString *NSSandBoxPublicPath() {
    return [NSSearchPathForDirectoriesInDomains(NSSharedPublicDirectory, NSUserDomainMask, YES) firstObject];
}


/// /System/Library/CoreServices
NSString *NSSystemCoreServicesPath() {
    return [NSSearchPathForDirectoriesInDomains(NSCoreServiceDirectory, NSSystemDomainMask, YES) firstObject];
}
/// /System/Library/Printers/PPDs
NSString *NSSystemPrintersPPDsPath() {
    return [NSSearchPathForDirectoriesInDomains(NSPrinterDescriptionDirectory, NSSystemDomainMask, YES) firstObject];
}
