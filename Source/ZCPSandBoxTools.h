//
//  ZCPSandBoxTools.h
//  ZCPUtil
//
//  Created by zcp on 2019/5/20.
//  Copyright © 2019 zcp. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/**
 1.FOUNDATION_EXPORT NSArray<NSString *> *NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory directory, NSSearchPathDomainMask domainMask, BOOL expandTilde);
 函数的最后一个参数expandTilde解释：
 YES：则值为详细路径，如/var/mobile/Containers/Data/Application/1B8A771C-63EA-413A-B9AD-89CA89593505/Documents
 NO： 则值为简写路径，如~/Documents
 
 2.真机和模拟器各自的Bundle路径和沙盒路径
 真机：
 Bundle路径：/var/containers/Bundle/Application/C2ED332B-A487-470E-A883-F30B5899EF90/Demo.app
 沙盒路径：/var/mobile/Containers/Data/Application/1B8A771C-63EA-413A-B9AD-89CA89593505
 
 模拟器：
 Bundle路径：/Users/zcp/Library/Developer/CoreSimulator/Devices/99119AAB-6E4C-4A78-B60B-3AF5421281C1/data/Containers/Bundle/Application/868ED7D7-07C0-4805-9A1F-B6F2138B17F3/Demo.app
 沙盒路径：/Users/zcp/Library/Developer/CoreSimulator/Devices/99119AAB-6E4C-4A78-B60B-3AF5421281C1/data/Containers/Data/Application/6E25838B-C241-4B71-9BE0-15D94EE6B155
 
 3.MainBundle各个path的值
 [[NSBundle mainBundle] bundlePath]：/var/containers/Bundle/Application/8CD24798-EE93-4D34-8078-4B5D0CDF8A44/Demo.app
 [[NSBundle mainBundle] resourcePath]：~
 [[NSBundle mainBundle] executablePath]：~/Demo.app/Demo
 [[NSBundle mainBundle] sharedSupportPath]：~/SharedSupport
 [[NSBundle mainBundle] builtInPlugInsPath]：~/PlugIns
 [[NSBundle mainBundle] sharedFrameworksPath]：~/SharedFrameworks
 [[NSBundle mainBundle] privateFrameworksPath]：~/Frameworks
 */

#pragma mark - sandbox

/// ~
FOUNDATION_EXTERN NSString *NSSandBoxHomePath(void);
/// /
FOUNDATION_EXTERN NSString *NSSystemRootPath(void);

/// ~/Documents
FOUNDATION_EXTERN NSString *NSSandBoxDocumentsPath(void);
/// ~/Library
FOUNDATION_EXTERN NSString *NSSandBoxLibraryPath(void);
/// ~/Library/Caches
FOUNDATION_EXTERN NSString *NSSandBoxCachesPath(void);
/// ~/Library/Preferences
FOUNDATION_EXTERN NSString *NSSandBoxPreferencesPath(void);
/// ~/Library/Application Support
FOUNDATION_EXTERN NSString *NSSandBoxApplicationSupportPath(void);
/// ~/Library/Documentation
FOUNDATION_EXTERN NSString *NSSandBoxDocumentationPath(void);
/// ~/Library/Autosave Information
FOUNDATION_EXTERN NSString *NSSandBoxAutosaveInformationPath(void);
/// ~/Library/Input Methods
FOUNDATION_EXTERN NSString *NSSandBoxInputMethodsPath(void);
/// ~/Library/PreferencePanes
FOUNDATION_EXTERN NSString *NSSandBoxPreferencePanesPath(void);
/// ~/tmp/
FOUNDATION_EXTERN NSString *NSSandBoxTmpPath(void);

/// ~/Applications
FOUNDATION_EXTERN NSString *NSSandBoxApplicationsPath(void);
/// ~/Applications/Demos
FOUNDATION_EXTERN NSString *NSSandBoxDemosPath(void);
/// ~/Applications/Utilities
FOUNDATION_EXTERN NSString *NSSandBoxUtilitiesPath(void);
/// ~/Developer/Applications
FOUNDATION_EXTERN NSString *NSSandBoxDeveloperApplicationPath(void);
/// ~/Developer
FOUNDATION_EXTERN NSString *NSSandBoxDeveloperPath(void);

/// ~/Desktop
FOUNDATION_EXTERN NSString *NSSandBoxDesktopPath(void);
/// ~/Downloads
FOUNDATION_EXTERN NSString *NSSandBoxDownloadsPath(void);
/// ~/Movies
FOUNDATION_EXTERN NSString *NSSandBoxMoviesPath(void);
/// ~/Music
FOUNDATION_EXTERN NSString *NSSandBoxMusicPath(void);
/// ~/Pictures
FOUNDATION_EXTERN NSString *NSSandBoxPicturesPath(void);
/// ~/Public
FOUNDATION_EXTERN NSString *NSSandBoxPublicPath(void);

NS_ASSUME_NONNULL_END
