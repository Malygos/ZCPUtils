//
//  ZCPUserDefaultsTool.m
//  ZCPUtil
//
//  Created by zcp on 2019/5/20.
//  Copyright © 2019 zcp. All rights reserved.
//

#import "ZCPUserDefaultsTools.h"

/// 保存
void SaveUserDefaults(id object, NSString *key) {
    [[NSUserDefaults standardUserDefaults] setObject:object forKey:key];
}
/// 读取
id ReadUserDefaults(NSString *key) {
    NSString *result = [[NSUserDefaults standardUserDefaults] objectForKey:key];
    if (!result) {
        return @"";
    }
    return result;
}
/// 删除
void RemoveUserDefaults(NSString *key) {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
}
/// 同步数据
void UserDefaultsSync(void) {
    [[NSUserDefaults standardUserDefaults] synchronize];
}
