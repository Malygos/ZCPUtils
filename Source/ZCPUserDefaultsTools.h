//
//  ZCPUserDefaultsTools.h
//  ZCPUtil
//
//  Created by zcp on 2019/5/20.
//  Copyright © 2019 zcp. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/// 保存
void SaveUserDefaults(id object, NSString *key);
/// 读取
id ReadUserDefaults(NSString *key);
/// 删除
void RemoveUserDefaults(NSString* key);
/// 同步数据
void UserDefaultsSync(void);

NS_ASSUME_NONNULL_END
